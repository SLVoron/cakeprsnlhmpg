-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 16 2015 г., 09:34
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cakePrsnlHmPg`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `message`) VALUES
(14, 'asd', 'fackhim@mail.ru', 'фываам'),
(18, 'name1', 'name1@mail.ru', 'name1 send to you hello'),
(19, 'name2', 'name2@mail.ru', 'name2 send to you hello'),
(20, 'name3', 'name3@mail.ru', 'name3 send to you hello'),
(21, 'name4', 'name4@mail.ru', 'name4 send to you hello'),
(34, '123', '123', '123'),
(35, '123', '123', '123'),
(36, '123', '123', '123'),
(37, '123', '123', '123'),
(38, '123', '123', '123'),
(39, '123', '123', '123'),
(40, '123', '123', '123'),
(41, '123', '123', '123'),
(42, '123', '123', '123'),
(43, '123', '123', '123'),
(44, '123', '123', '123'),
(45, '123', '123', '123'),
(46, '123', '123', '123'),
(47, '123', '123', '123'),
(48, '123', '123', '123'),
(49, '123', '123', '123'),
(50, '123', '123', '123'),
(51, '123', '123', '123'),
(52, '123', '123', '123'),
(53, '123', '123', '123'),
(54, '123', '123', '123'),
(55, '123', '123', '123'),
(56, '123', '123', '123'),
(57, '123', '123', '123'),
(58, '123', '123', '123'),
(59, '123', '123', '123'),
(60, '123', '123', '123'),
(61, '123', '123', '123'),
(62, '123', '123', '123'),
(63, '123', '123', '123'),
(64, '43t3', '3rt3t@gtrhgtr.yuk', '5657'),
(65, '43t3', '3rt3t@gtrhgtr.yuk', '5657');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`) VALUES
(1, 'Admin', 'admin', '2015-10-14 20:43:01');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
