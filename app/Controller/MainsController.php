<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: SLVor
 * Date: 15.10.2015
 * Time: 15:12
 */
class MainsController extends AppController {
    public function index() {
        $this->set('mains');
        $this->set('title_for_layout', 'Это главная страница');
    }
}