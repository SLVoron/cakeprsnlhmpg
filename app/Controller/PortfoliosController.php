<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: SLVor
 * Date: 15.10.2015
 * Time: 15:12
 */

class PortfoliosController extends AppController {
    public function index() {
        $this->set('portfolios');
        $this->set('title_for_layout', 'Портфолио');
    }
}