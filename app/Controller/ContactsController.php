<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: SLVor
 * Date: 15.10.2015
 * Time: 15:48
 */
class ContactsController extends AppController {
    public  $uses=array('Message');
    public function index() {
        $this->set('contacts');
        $this->set('title_for_layout', 'Контакты');

        if ($this->request->is('post')) {
            if ($this->Message->save($this->request->data)) {
                $this->Flash->success(__('Ваше сообщение было отправлено'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('Сообщение не отправилось, попробуйте еще раз.'));
            }
        }

    }
}