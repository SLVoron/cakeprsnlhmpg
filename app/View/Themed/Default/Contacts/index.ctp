<div class="container index-page">
    <div class="row">
        <div class="col-lg-12">
            <h2>Меня можно найти по ВКонтакте и Скайпе</h2>
            <p>Я <a href="vk.com/voronenkosl">ВКонтакте</a></p>
            <p>Мой скайп slvoronenko</p>
            <p>Также вы можете заполнить данную форму и написать мне сообщение, я его обязательно прочитаю и, возможно, отвечу вам =)</p>
        </div>
    </div>
<?php
echo $this->Form->create('Message');
echo $this->Form->input('name', array('label' => 'Введите ваше имя'));
echo $this->Form->input('email', array('label' => 'Введите ваш Email'));
echo $this->Form->input('message', array('rows' => '5', 'label' => 'Введите ваше сообщение'));
echo $this->Form->end(__('Отправить сообщение'));
?>
</div>