<div class="container index-page">
    <div class="row">
        <ul class="nav nav-pills">
          <li role="presentation"><?php echo $this->Html->link('Главная', array('controller'=>'mains', 'action' => 'index'));?></li>
          <li role="presentation"><?php echo $this->Html->link('Портфолио', array('controller'=>'portfolios', 'action' => 'index'));?></li>
          <li role="presentation"><?php echo $this->Html->link('Контакты', array('controller'=>'contacts', 'action' => 'index'));?></li>
        </ul>
    </div>
</div>