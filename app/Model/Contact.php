<?php
/**
 * Created by PhpStorm.
 * User: SLVor
 * Date: 15.10.2015
 * Time: 15:47
 */
class Contact extends AppModel {
    public $validate = array(
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => '������� ���������� ����� E-mail!',
                //'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'message' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => '������� ���� ���-������!',
                //'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
}